#!/bin/bash
#A Shell Script for User Management like Create, Delete, Modify User

clear
choice=0
while [ $choice -lt 5 ]
 do
  #Providing List of Features to the User
   echo "Please choose the options:"
   echo "1) Create a new User"
   echo "2) Delete User"
   echo "3) Modification of User"
   echo "4) Exit"
   read choice

 case $choice in
      #Creating new user
      1) echo " "
         echo "Creating a new User"
         echo " "
         #Searching User already exists or not
	      read -p    "Enter username : " username
	      read -s -p "Enter password : " password
         #/dev/null to ignore any unwanted/actual output from command
         #Try to locate username in /etc/passwd
	      egrep "^$username" /etc/passwd >/dev/null
	      if [ $? -eq 0 ]; 
            then
		       tput setaf 5; tput bold; "$username user already exists!"
		       exit 1
	         else
		       pass=$(perl -e 'print crypt($ARGV[0], "password")' $password)
		       sudo useradd -m -p $pass $username
		       [ $? -eq 0 ] 
             echo " "
             tput setaf 3; tput bold; echo "User has been added to system!"
	      fi
         exit 2
	   ;;

     #Delete user from system
	  2)  echo " "
         echo "You want to delete user"
         echo " "
	      read -p "Please enter username : " username
         #Try to locate username in /etc/passwd
	      egrep "^$username" /etc/passwd >/dev/null
	      if [ $? -ne 0 ]; 
            then
	          tput setaf 5; tput bold; echo "$username user doesn't exists!"
		       exit 1
	         else
	          sudo userdel $username
             tput setaf 1; tput bold; echo "$username user deleted"
         fi
	      exit 2
     ;; 

	  3)  echo " "
	      echo "User Modification as adding UID of Existing User"
		   echo " "
	      read -p "Enter username : " username
         #Try to locate username in /etc/passwd
	      egrep "^$username" /etc/passwd >/dev/null
	      if [ $? -ne 0 ]; 
            then
	          tput setaf 5; tput bold; echo "$username user doesn't exists!"
		       exit 1
	         else
	          sudo usermod -u 2005 $username
             tput setaf 3; tput bold; echo "$username user_id changes"
         fi
	      exit 2
	  ;;

     4)  echo " "
         tput setaf 6; tput bold; echo "About to Exit"
	      echo " "
	      exit
     ;;

     *)
        tput setaf 1; tput bold; echo "Please Pass Valid Argument"
        exit
     ;;	  	 
 esac  
done

