#!/bin/bash
#A Shell Script for directory management like creation, modification, navigate into directory
clear
choice=0
while [ $choice -lt 6 ]
 do
  #Providing List of Features to the User
  echo "Press the follwing and Hit ENTER to do:"
  echo "1) Create a new Directory"
  echo "2) Modification of Directory"
  echo "3) Listing Directories and Files"
  echo "4) Navigate to specific Directory"
  echo "5) Exit"
  read choice

  case $choice in
     1) echo " "
        echo "Creating a new Directory"
        echo " "
        echo "Enter the name for your Directory"
        echo " "
        read  directory_name
        mkdir -p $directory_name
        tput setaf 3; tput bold; echo "Your directory named $directory has been created"
     ;;

     2) echo " "
        echo "Modification of Directory"
        echo " "
        echo "Please enter the Directory name which you want to Modify"
        echo " "
        read original_directory
        echo "Choose the following Options to do so:"
        echo " "
        echo "1) Rename Your Directory"
        echo "2) Copy Your Directory"
        echo "3) Move Your Directory"
        echo "4) Delete Your Directory"
        echo "5) Exit from this Stage"
        read     modification_option
              
        case $modification_option in
          1) echo " "
             echo "Rename Your Directory"
             echo " "
             tput setaf 6; tput bold; echo "Please Enter new name for your Directory"
             read new_name
             sudo mv $original_directory $new_name
             tput setaf 3; tput bold; echo "You $original_directory directory has been renamed"
          ;;

          2) echo " "
             echo "Copy Your Directory"
             echo " "
             tput setaf 6; tput bold; echo "Please put target to copy your Directory"
             read target
             sudo cp -a $original_directory $target
             tput setaf 3; tput bold; echo "You directory has been copied"
          ;;

          3) echo " "
             echo "Move Your Directory"
             echo " "
             tput setaf 6; tput bold; echo "Please put target to move your Directory"
             read target
             sudo mv -v $original_directory $target
             tput setaf 3; tput bold; echo "You $original_directory directory has been moved"

          ;;

          4) echo " "
             echo "Delete Your Directory"
             echo " "
             sudo rm -rf $original_directory
             tput setaf 3; tput bold; echo "Your directory has been deleted"
          ;;

          5) echo " "
             echo "Exiting from Modification Mode"
             exit
          ;;

          *)
             tput setaf 1; tput bold; echo "Please Pass Valid Argument"
             exit
          ;; 
        esac
     ;;

     3) echo " "
        echo "Listing Directories and Files"
        echo " "
        echo "Enter Your choice for Method of Listing"
        echo "1) List of Directories and Files only"   
        echo "2) List of Directories and Files with their details"
        echo "3) List along with Hidden Directories and Files"
        echo "4) Exit from Listing Mode"
        read  listing_choice

        case $listing_choice in
		     1) echo " "
		        echo ""
		        echo "List of Directories and Files only"
		        ls
		     ;; 

		     2) echo " "
		        echo "List of Directories and Files with their details"
		        echo " "
		        ls -l 
		     ;;

           3) echo " "
               echo "List along with Hidden Directories and Files"
               echo " "
               ls -la
           ;;

		     4) echo " "
		         tput setaf 4; tput bold; echo "Exit from Listing Mode"
		         echo " "
		         exit
		     ;; 

           *)
             tput setaf 1; tput bold; echo "Please Pass Valid Argument"
             exit
           ;; 
		  esac
     ;;

     4) echo " "
        echo "Navigation into Directory"
	     echo " "
	     echo "Enter your choice for method of Navigation :"
	     echo "1) Go to Parent Directory. "
	     echo "2) Navigate to specific Directory."
	     echo "3) Exit from Navigate Mode."
	     read  navigation_choice
		
	     case $navigation_choice in
	        1) echo " "
		        echo "Parent Directory"
		        echo " "
              cd
		        pwd
		     ;; 

		     2) echo " "
		        echo "Navigation to Specific Directory"
		        echo " "
		        tput setaf 6; tput bold; echo "Enter the target Path:"
		        read path 
		        cd   $path 
		        pwd
              ls -l
		     ;; 

		     3) echo " "
		        tput setaf 4; tput bold; echo "Exiting from Navigate Mode"
		        echo " "
		        exit
		     ;; 

           *)
              tput setaf 1; tput bold; echo "Please Pass Valid Argument"
              exit
           ;;
		  esac
	  ;;    

     5) echo " "
        tput setaf 4; tput bold; echo "About to Exit"
	     echo " "
	     exit
     ;;

     *)
        tput setaf 1; tput bold; echo "Please Pass Valid Argument"
        exit
     ;;
  esac
done