#!/bin/bash
# A Shell Script for Group Management like Create, Delete, Modify group

clear
choice=0
while [ $choice -lt 5 ]
 do
  #Providing List of Features to the User
  echo "Please choose the options:"
  echo "1) Create a new Group"
  echo "2) Delete Group"
  echo "3) Modification of Group"
  echo "4) Exit"
  read choice

 case $choice in
   1) echo " "
      echo "Creating a new Group"
      echo " "
	    read -p "Enter groupname : " groupname

      #Try to locate groupname in /etc/group
      #/dev/null to ignore any unwanted/actual output from command
	    egrep "^$groupname" /etc/group >/dev/null
	   if [ $? -eq 0 ]; 
        then
	       tput setaf 5; tput bold; echo "$groupname already exists!"
		     exit 1
	      else
	       sudo groupadd  $groupname
	       [ $? -eq 0 ] 
         tput setaf 3; tput bold; echo "$groupname Group has been added to system!"
     fi
	   exit 2
   ;;

   2) echo " "
      echo "Delete Your Group"
      echo " "
	    read -p "Enter groupname : " groupname

      #Try to locate groupname in /etc/group
	    egrep "^$groupname" /etc/group >/dev/null
	   if [ $? -ne 0 ]; 
        then
	       tput setaf 5; tput bold; echo "$groupname group Doesn't exists!"
		     exit 1
	      else
	       sudo groupdel $groupname
         tput setaf 1; tput bold; echo "$groupname group deleted from system"
     fi
	   exit 2
   ;; 

   3) echo " "
      echo "Modification of Group by adding user"
      echo " "
	    read -p "Enter groupname : " groupname
      egrep "^$groupname" /etc/group >/dev/null
	   if [ $? -ne 0 ]; 
        then
	       tput setaf 5; tput bold; echo "$groupname group Doesn't exists!"
		     exit 1
        else
         read -p "Enter username : " username
         egrep "^$username" /etc/passwd >/dev/null
       if [ $? -ne 0 ]; 
          then
	         tput setaf 5; tput bold; echo "$username user Doesn't exists! If you want to add it Please Create it first"
		       exit 2
          else
           echo "Adding user $username to group $groupname"
           sudo usermod -a -G $groupname $username
           echo " "
           tput setaf 3; tput bold; echo "$username user added to $groupname group"
       fi
     fi   
   ;;   

   5) echo " "
      tput setaf 4; tput bold;echo "About to Exit"
	    echo " "
	    exit
   ;;

   *)
      tput setaf 1; tput bold; echo "Please Pass Valid Argument"
      exit
   ;;
 esac
done