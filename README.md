# Linux Assignment

### Create a utility to manage users. It should cover below functionalities: ###

*  User Creation
*  User Deletion
*  User Modification


#### Script Functionality ####

##### Here I am Provided Options to User to do so: #####

1.  *Create New User*
2.  *Delete User*
3.  *Modification of User*
4.  *Exit from Option mode*

**Create New User**

Take Username as Input and searched either user already exist or not in **/etc/passwd** file 
If User found in **/etc/passwd** file then I print ***User already exists*** if user not found then easily create user using **$ sudo useradd -m -p *password* *username***
here **-m** for adding home directory for new user and **-p** is for adding password.


**Delete User**

For this I checked again on ***/etc/passwd*** file . If User exists in file then use command **$ sudo userdel *username***. 

**User Modification**

For this I tried to change ***U_ID*** by using **sudo usermod u_id *username*** but this will done only if user already exists on ***/etc/passwd*** otherwise print User does not exist.



### Create the utility to manage groups. It should cover below functionalities: ###
*  Group Creation
*  Group Deletion
*  Group Modification

#### Script Functionality ####

##### Here I am Provided Options to User to do so: #####

1.  *Create New Group*
2.  *Delete Group*
3.  *Modification of Group*
4.  *Exit from Option mode*

**Create New Group**

Take Groupname as Input and searched either group already exist or not in **/etc/group** file 
If group name found in **/etc/group** file then I print ***Group already exists*** if group not found then easily create group using **$ sudo groupadd *groupname***


**Delete Group**

For this I checked again on ***/etc/group*** file . If group exists in file then use command **$ sudo groupdel *groupname***. 

**Group Modification**

For this I tried to add user in group iff and only if user already exist otherwise print *User does not exist please add user first*.



### Create the utility to manage files. It should cover below functionalities ###
*  File Creation
*  File Modification
*  File Deletion
*  Navigate to File

#### Script Functionality ####

##### Here I am Provided Options to User to do so: #####
1.  *Create File*
2.  *Modification of file*
3.  *Navigate to file*
4.  *Exit from option mode*

Use **$ touch *filename*** to create file.
For Modification of file I used to to as Delete file, Copy file, Rename file, move file, Add content on your file.
To navigate on file used  **$cat *filename***



### Create the utility to manage directory. It should cover below functionalities ###
*  Directory creation
*  Directory modification
*  Directory deletion

#### Script Functionality ####


##### Here I am Provided Options to User to do so: #####
1.  *Create Directory*
2.  *Modification of Directory*
3.  *Navigate to file*
4.  *Listing directories and files*
5.  *Exit*


To create directory simple use **$ mkdir -p dir_name**
For modification of directory I used to do rename, copy of directory, move directory, delete directory.
Navigated by listing the directory to see the content inside the directory.



### Create the utility to manage Package. It should cover below functionalities:(It should be cove Debian and RedHat OS family) ###

*  Repository Updation
*  Package installation
*  Upgradation
*  Uninstallation

For this first I checked the OS from **./etc/OS-release** if OS is ***Ubuntu*** then use **sudo apt-get** and if OS is ***RedHat*** then use **yum**




### Update the utility to manage services. It should cover below functionalities: ###

*  Service start
*  Service re-start
*  Service stop
*  Systemd daemon reload


### Install Nginx and tomcat ###


### There should be a different tomcat user to manage tomcat configuration ###

For this I created a new user to tomcat group using **sudo useradd -g users -G *tomcat* *tomcat_user*** if user already exist by this name then notified to user that user already exist try to add different user.




### Nginx default HTML page should be like Hi My name is $name_of_yours ###

For this first I get to know about either **nginx** is installed or not if Nginx is installed then **sudo sed -i 's/Welcome to nginx!/Welcome to PRATAP Galaxy/' /var/www/html/index.nginx-debian.html**



### After any configuration change nginx service should be restarted ###

For this task I track status of modification timestamp of **nginx.conf** file by using **stat -c %y /etc/ngin/nginx.conf** and make a variable to take care for present config. file modification access timestamp.
If both timestamp are equal then no need to re-start service but if both timestamps are not equal then re-start nginx service by **$ sudo systemctl nginx restart**.

