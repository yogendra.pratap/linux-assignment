#!/bin/bash
#Shell Script to Add a specific tomcat User
echo "Creating a new User"
echo " "
read -p    "Enter username : " username
read -s -p "Enter password : " password
#/dev/null to ignore any unwanted/actual output from command
#Try to locate username in /etc/passwd
egrep "^$username" /etc/passwd >/dev/null
if [ $? -eq 0 ]; 
   then
	 echo "user $username already exists!"
	 exit 1
   else
	 pass=$(perl -e 'print crypt($ARGV[0], "password")' $password)
	 sudo useradd -g users -G tomcat tomcat_user
	 [ $? -eq 0 ] 
	 tput setaf 3; tput bold; echo "User has been added to system!"
   exit 2
fi