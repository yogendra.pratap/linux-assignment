#!/bin/bash
# A Shell Script for file management like creation, modification, navigate into file
clear
choice=0
while [ $choice -lt 5 ]
 do
   #Providing List of Features to the User
   echo "Please choose the options:"
   echo "1) Create a new File"
   echo "2) Modification of File"
   echo "3) Navigate to specific File"
   echo "4) Exit"
   read choice

   case $choice in
      1) echo " "
         echo "Creating a new File"
         echo " "
         tput setaf 6; tput bold; echo "Enter the name for your File"
         echo " "
         read  file_name
         touch $file_name
         tput setaf 3; tput bold; echo "$file_name file created"
      ;;

      2) echo " "
         echo "Modification of File"
         echo " "
         ls -l
         echo " "
         tput setaf 6; tput bold; echo "Please enter the File name which you want to Modify"
         read  original_filename

         echo " "
         echo "Choose the following Options to do so:"
         echo " "
         echo "1) Rename Your File"
         echo "2) Copy Your File"
         echo "3) Move Your File"
         echo "4) Delete Your File"
         echo "5) Add Content on File"
         echo "6) Exit from this Stage"
         read  modification_option
                    
           case $modification_option in
              1) echo " "
                 echo "Rename Your File"
                 echo " "
                 tput setaf 6; tput bold; echo "Please Enter new name for your File"
                 read new_filename
                 sudo mv $original_filename $new_filename
                 tput setaf 3; tput bold; echo "$original_filename file renamed as $new_filename file"
              ;;

              2) echo " "
                 echo "Copy Your File"
                 echo " "
                 ls -l
                 echo " "
                 tput setaf 6; tput bold; echo "Which File You want to Copy"
                 read desired_file
                 echo " "
                 tput setaf 6; tput bold; echo "Please put target to copy your File"
                 read target
                 sudo cp $desired_file $target
                 tput setaf 3; tput bold; echo "Your file $desired_file is copied"
              ;;

              3) echo " "
                 echo "Move Your File"
                 echo " "
                 ls -l
                 tput setaf 6; tput bold; echo "Which File You want to Move"
                 read desired_file
                 echo " "
                 tput setaf 6; tput bold; echo "Please put target to move your File"
                 read target
                 sudo mv $desired_file $target
                 tput setaf 3; tput bold; echo "Your file $desired_file moved"
              ;;

              4) echo " "
                 echo "OOPS do you want to Delete Your File"
                 echo " "
                 ls -l
                 echo " "
                 tput setaf 6; tput bold; echo "Which File You want to Delete"
                 echo " "
                 read desired_filename
                 sudo rm $desired_filename
                 tput setaf 1; tput bold; echo "Your file $desired_filename is deleted"
              ;;

              5) echo " "
                 echo "Add Content on File"
                 echo " "
                 sudo vim $original_filename
                 tput setaf 3; tput bold; echo "You have added content on your $original_filename"
              ;;

              6) echo " "
                 tput setaf 4; tput bold; echo "Exiting from Modification Mode"
                 exit
              ;;

              *)
                 tput setaf 1; tput bold; echo "Please Pass Valid Argument"
                 exit
              ;; 
           esac
      ;;

      3) echo " "
         echo "Navigation into File"
	      echo " "
	      echo "Enter your choice for method of Navigation :"
	      echo "1) View File. "
	      echo "2) Exit from Navigate Mode."
	      read  navigation_choice
		
	      case $navigation_choice in
		      1) echo " "
		         echo "Viewing File"
		         echo " "
               ls -l
               echo " "
               tput setaf 6; tput bold; echo "Which File You Want to Show"
               read show_file
               echo " "
		         cat  $show_file
		      ;; 

		      2) echo " "
		         tput setaf 4; tput bold; echo "Exiting from Navigation Mode"
		         echo " "
		         exit
		      ;; 

            *)
               tput setaf 1; tput bold; echo "Please Pass Valid Argument"
               exit
            ;;
		   esac
	   ;;    

      5) echo " "
         tput setaf 4; tput bold; echo "About to Exit"
         echo " "
	      exit
      ;;

      *)
         tput setaf 1; tput bold; echo "Please Pass Valid Argument"
         exit
      ;;
   esac
done