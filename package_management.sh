#!/bin/sh
#A Script Which manages the Package Update, Installation, Upgradation, Uninstallation
. /etc/os-release

echo "Your OS is $ID"
echo " "

choice=0
while [ $choice -lt 6 ]
 do
  #Providing List of Features to the User
   echo "Press the follwing task and Hit ENTER to do:"
   echo "1) Repository Update"
   echo "2) Package Installation"
   echo "3) Upgradation"
   echo "4) Uninstallation"
   echo "5) Exit"
   read choice

    case $choice in
      1) echo " " 
         tput setaf 3; tput bold; echo "Updating Repository"
         if [ $ID == ubuntu ]
            then
              sudo apt-get update
            else 
              sudo yum update  
         fi
      ;;

      2) echo " "
         echo "Which Package You Want Nginx or Tomcat"
         read  packg_name
         
         which $packg_name > /dev/null
         if [ $? -ne 0 ]
            then
             
             if [ $ID == ubuntu ]
                then
                sudo apt install $packg_name 
                tput setaf 3; tput bold; echo "Your $packg_name package has been Installed"             
           
                else
                yum install $packg_name
                tput setaf 3; tput bold; echo "Your $packg_name package has been Installed"
             fi    
            else 
             tput setaf 1; tput bold; echo "You already have this Package"    
         fi
      ;;

        3) echo " "
           tput setaf 3; tput bold; echo "Upgrading Your Packages"
           echo " "

           if [ $ID == ubuntu ]
              then
                sudo apt-get upgrade
              else 
                yum check update
           fi
        ;;

        4) echo " "
           echo "Which Package You Want to Uninstall"
           read  packg_name
           which $packg_name > /dev/null
           if [ $? -ne 0 ]
              then
                 if [ $ID == ubuntu ]
                   then
                      sudo apt remove $packg_name 
                      tput setaf 3; tput bold; echo "Your $packg_name package has been Uninstalled"             
                   else
                      yum remove $packg_name
                      tput setaf 3; tput bold; echo "Your $packg_name package has been Installed"
                 fi    
              else 
                 tput setaf 1; tput bold; echo "You don't have this Package"    
           fi
        ;;

        5) echo " "
           tput setaf 4; tput bold; echo "About to Exit"
	        echo " "
	        exit
        ;;

        *)
           tput setaf 1; tput bold; echo "Please Pass Valid Argument"
           exit
        ;;   
    esac
done   