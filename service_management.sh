#!/bin/bash
# A Script which Manages the Service Status like Start, Stop, Re-Start
clear
choice=0
while [ $choice -lt 6 ]
 do
  #Providing List of Features to the User
   echo "Please choose the options:"
   echo "1) Start Service"
   echo "2) Stop Service"
   echo "3) Service Re-Start"
   echo "4) System Daemon Reload"
   echo "5) Exit"
   read choice

   case $choice in
      1) echo " "
         echo "You Choose to Start Service Option"
         echo " "
         tput setaf 6; tput bold; echo "Which Service do you want to Start "
         read service_name
         service=$service_name
         service_status=dead
         systemctl status $service | grep -i 'running\|dead' | awk '{print $3}' | sed 's/[()]//g' | 
         while read status;
             do
             tput setaf 2; tput bold; echo "Your Service is: $status"
             echo " "
               if [ "$status" == "$service_status" ];
                  then
                  systemctl start $service
                  tput setaf 3; tput bold; echo "$service service is UP now"
                  else
                  tput setaf 5; tput bold; echo "$service service is Running Already"
               fi
          done  
      ;;  

      2) echo " "
         echo "You Choose to Stop Service Option"
         echo " "
         tput setaf 6; tput bold; echo "Which Service do you want to Stop "
         read service_name
         service=$service_name
         service_status=running
         systemctl status $service | grep -i 'running\|dead' | awk '{print $3}' | sed 's/[()]//g' | 
         while read status;
             do
             tput setaf 2; tput bold; echo "Your Service is: $status"
             echo " "
              if [ "$status" == "$service_status" ];
                 then
                 systemctl stop $service
                 tput setaf 3; tput bold; echo "$service service is DOWN now"
                 else
                 tput setaf 5; tput bold; echo "$service service is Already Inactive"
              fi
          done  
      ;;  

      3) echo " "
         echo "Which Service do You want to Re-Start"
         read service_name
         service $service_name status | grep 'active (running)' > /dev/null 2>&1
          if [ $? != 0 ]
             then
             sudo service $service_name restart > /dev/null
             tput setaf 3; tput bold; echo "$service_name service re-started"
          fi
      ;;  

      4) echo " "
         echo "Which Service You want to use as Daemon Reload"
         read service_name
         echo " "
         echo "Performing System Daemon Reload"
         sudo systemctl status $service_name
      ;; 

      5) echo " "
         tput setaf 4; tput bold; echo "About to Exit"
         exit
      ;;

      *)
         tput setaf 1; tput bold; echo "Please Pass Valid Argument"
         exit
      ;;  
   esac   
done