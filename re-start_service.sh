#!/bin/bash
#Shell Script to re-start service if any Changes occur on config file
tput setaf 6; tput bold;echo "Which Service do you want to Re_start"
echo " "
read service_name 
if [ -f status_file ]
   then
    modification_log=$(stat -c %y /etc/"$service_name"/"$service_name".conf)
    echo "$modification_log"
        if [ -f "/etc/"$service_name"/"$service_name".conf" ]
           then
               if [[ $(cat status_file) == "$modification_log" ]]
                  then 
                    tput setaf 5; tput bold; echo "No changes done in config file"
                  else
                    echo "Changes are detected"
                    sudo service $service_name restart
                    echo "$modification_log" > status_file
                    echo " "
                    tput setaf 3; tput bold; echo "$service_name Service Re-Started"
               fi    
           else
            tput setaf 1; tput bold; echo "Config File Not Found" 
        fi
   else
    stat -c %y /etc/"$service_name"/"$service_name".conf > status_file
fi
